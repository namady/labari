import os
import jinja2
import json
import datetime
import time
from google.appengine.ext import db
from google.appengine.api import memcache
from datetime import datetime
from threading import Thread
from appengine_config import translate
from google.appengine.api.mail import AdminEmailMessage
from model import Labari, News
from google.appengine.api import mail

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'isoformat'): #handles both date and datetime objects
            return obj.isoformat()
        else:
            return json.JSONEncoder.default(self, obj)

def _send_invite_email(invite, code):
	message_to = "%s <%s>" % (invite.name, invite.key.id())
	message_subject = 'Invitation to become Admin at Atakaice'
	message_content = """
	Hello %s,

	You are invited to become an admin at Atakaice Inc. https://atakaice.com, 
	click or copy and paste the link below in a browser to set your account up.

	https://atakaice.com/admin/signup?code=%s

	Regards,
	Atakaice Inc. """ % (invite.name, code)
	mail.send_mail(sender = 'landhausa@gmail.com',
    	to = message_to,
    	subject = message_subject,
    	body = message_content)

def _template_render(template_name, **context):
		jinja_env = jinja2.Environment (
			loader = jinja2.FileSystemLoader(os.path.dirname(__file__)))
		if context is None:
			context = {}

		template = jinja_env.get_template('templates/'+str(template_name))
		return template.render(context)

def _get_fresh_news():
	#labari = db.GqlQuery("SELECT * FROM Labari ORDER BY pubDate DESC LIMIT 10")
	labari_list, labari_cursor, more = Labari.query().order(-Labari.pubDate).fetch_page(10)

	memcache.set('labari_cursor', labari_cursor)
	return labari_list

def _get_news():
	#labari = db.GqlQuery("SELECT * FROM Labari ORDER BY pubDate DESC LIMIT 4")
	#labari_list, labari_cursor, more = Labari.query().order(-Labari.pubDate).fetch_page(4)
	labari_cursor = memcache.get('labari_cursor')
	labari_list, labari_cursor, more = Labari.query().order(-Labari.pubDate).fetch_page(4, start_cursor=labari_cursor)

	memcache.set('labari_cursor', labari_cursor)
	return labari_list

def to_dict(model):
    output = dict()
    output['title'] = model.title
    output['pubDate'] = model.pubDate
    output['description'] = model.description
    output['files'] = model.files
    output['source'] = model.source
    output['author'] = model.author

    return output

def _get_json_news(count = 4):
	labari_cursor = memcache.get('labari_cursor')
	labari_list, labari_cursor, more = Labari.query().order(-Labari.pubDate).fetch_page(count, start_cursor=labari_cursor)

	memcache.set('labari_cursor', labari_cursor)
	labari_list = list(map(to_dict, labari_list))
	return json.dumps(labari_list, cls=JSONEncoder)
			
def _get_single_news_ajax(id):
	key = db.Key.from_path('Labari', int(id))
	obj = db.get(key)

	if obj:
		single_news = dict([(p, getattr(obj, p)) for p in obj.properties()])
		return json.dumps(single_news, cls=JSONEncoder)

def _get_channel_news(channel):
	labari = db.GqlQuery("SELECT * FROM Labari WHERE source = " + channel + "ORDER BY pubDate DESC LIMIT 10")
	cursor_name = channel + '_cursor'

	labari_cursor = memcache.get(cursor_name)

	if labari_cursor:
		labari.with_cursor(start_cursor=labari_cursor)

	result = []
	for item in labari:
            result.append(dict([(p, getattr(item, p)) for p in item.properties()]))
        labari_cursor = labari.cursor()
        memcache.set(cursor_name, labari_cursor)
        return json.dumps(result, cls=JSONEncoder)

def process(date):
	now = datetime.now()
	minutes = int((now - date).total_seconds() / 60)
	hours = 0
	days = 0
	months = 0

	if minutes < 60:
		if minutes < 2:
			return "a minute ago", minutes
		return str(minutes) + " minutes ago", minutes
	else:
		hours = int(minutes / 60)

	if hours < 24:
		if hours < 2:
			return "an hour ago", minutes
		return str(hours) + " hours ago", minutes
	else:
		days = int(hours / 24)

	if days < 29:
		if days < 2:
			return "1 day ago", minutes
		return str(days) + " days ago", minutes
	else:
		months = int(days / 29)

	if months < 2:
		return "1 month ago", minutes
	return str(months) + " months ago", minutes



	