from forms import *
from google.appengine.ext import ndb

class Admin(ndb.Model):
	name = ndb.StringProperty(required=True)
	phone = ndb.StringProperty(required=False)
	password = ndb.StringProperty(required=True)
	level = ndb.IntegerProperty(default=2)
	photo = ndb.StringProperty(required=False)
	created_at = ndb.DateTimeProperty(auto_now_add=True)
	updated_at = ndb.DateTimeProperty(auto_now=True)

	@classmethod
	def by_id(cls, uid):
		return Admin.get_by_id(uid)

	@classmethod
	def by_name(cls, name):
		u = Admin.all().filter('name =', name).get()
		return u

	def done(self, *a, **kw):
		raise NotImplementedError

	@classmethod
	def register(cls, name, password, email):
		pw_hash = make_pw_hash(email, password)
		return Admin(name = name,
                    password = pw_hash,
                    id = email)

	@classmethod
	def login(cls, email, password):
		admin = cls.by_id(email)
		if admin and validate_user_password(email, password, admin.password):
			print 'user found', admin.name
			return admin