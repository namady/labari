from forms import *
from google.appengine.ext import ndb

class Invitation(ndb.Model):
	name = ndb.StringProperty(required=True)
	accept = ndb.BooleanProperty(default=False)
	level = ndb.IntegerProperty(default=3)
	code = ndb.StringProperty(required=True)
	created_at = ndb.DateTimeProperty(auto_now_add=True)

	@classmethod
	def by_id(cls, email):
		return Invitation.get_by_id(email)

	@classmethod
	def by_code(cls, code):
		return cls.query().filter(cls.code == code).get()