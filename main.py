#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import json
from helpers import _template_render
from model import _store_news
from helpers import _get_news
from helpers import process
from helpers import _get_fresh_news
from helpers import _get_json_news
from helpers import _get_single_news_ajax
from helpers import _send_invite_email
from helpers import JSONEncoder
from model import Labari, News
from google.appengine.ext import db, ndb
from google.appengine.ext.blobstore import blobstore
from pybcrypt import bcrypt
from admin import Admin
from invitation import Invitation
from forms import *
from google.appengine.api import app_identity
from google.appengine.api import mail
from google.appengine.api import channel
import cloudstorage
import mimetypes
import socket
import os

# seed_admin = Admin.register("Muawiyah", "namady", "ceo@namady.com")
# seed_admin.level = 1
# seed_admin.put()
# seed_admin = Admin.register("BBC", "namady", "bbc")
# seed_admin.level = 1
# seed_admin.put()
# seed_admin = Admin.register("VOA", "namady", "voa")
# seed_admin.level = 1
# seed_admin.put()
# seed_admin = Admin.register("RFI", "namady", "rfi")
# seed_admin.level = 1
# seed_admin.put()

class HelperHandler(webapp2.RequestHandler):
    def write(self, *a, **kwargs):
        self.response.write(*a, **kwargs)

    def render(self, template, **kwargs):
        self.write(_template_render(template, **kwargs))

    def done(self, *a, **kw):
        raise NotImplementedError

    def delete_cookie(self, name):
        self.response.delete_cookie(name)

class MainHandler(HelperHandler):
    def get(self):
        params = dict(title = 'Labarun Duniya', labarai = _get_fresh_news())
        self.render('main.html', **params)

    def post(self):
        name = self.request.POST.get('name')
        phone = self.request.POST.get('phone')
        email = self.request.POST.get('email')
        user_msg = self.request.POST.get('message')
        message_content = 'From : ' + name + '\nEmail : ' + email + '\nPhone : ' + phone + '\n\n' + user_msg

        mail.send_mail(sender='landhausa@gmail.com',
                   to="Mu'awiyah Namadi <ceo@namady.com>",
                   subject="From Atakaice Contact Page",
                   body=message_content)
        self.response.write("message sent")

class MainHandlerAjax(HelperHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        limit = int(self.request.get('limit'))
        if limit:
            self.response.out.write(_get_json_news(count = limit))
        else:
            self.response.out.write(_get_json_news())

class InfiniteScroll(HelperHandler):
    def get(self):
        params = dict(title = 'Labarun Duniya', labarai = _get_news())
        self.render('main.html', **params)

class AboutHandler(HelperHandler):
    def get(self):
        self.render('about.html')

    def post(self):
        pass

class ContactHandler(HelperHandler):
    def get(self):
        self.render('contact.html')

    def post(self):
        pass

class GetNewsHandler(HelperHandler):
	def get(self):
		_store_news()

	def post(self):
		_store_news()

class AdminInvite(HelperHandler):
    def get(self):
        self.redirect('/admin/board')

    def post(self):
        self.response.headers['Content-Type'] = 'application/json' 
        name = str(self.request.params.get('name'))
        email = str(self.request.params.get('email'))
        level = int(self.request.params.get('level'))
        code = make_salt(30)

        invite = Invitation.by_code(code)
        while invite:
            code = make_salt(30)
            invite = Invitation.by_code(code)

        admin = Admin.by_id(email)
        if admin:
            params = dict(status_msg = 'admin with this email already exist')
            self.response.set_status(300)
            self.write(json.dumps(params, cls=JSONEncoder))
            return

        invite = Invitation.by_id(email)
        if invite:
            params = dict(status_msg = 'admin has already been invited')
            self.response.set_status(300)
            self.write(json.dumps(params, cls=JSONEncoder))
        else:
            invite = Invitation(name = name, id = email, level = level, code = code)
            invite.put()
            params = dict(status_msg = 'invitation sent sucessfully')
            _send_invite_email(invite, code)
            self.write(json.dumps(params, cls=JSONEncoder))
                
class AdminHandler(HelperHandler):
    def set_secure_cookie(self, email, val):
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (email, cookie_val))

    def read_secure_cookie(self, email):
        cookie_val = self.request.cookies.get(email)
        return cookie_val and check_secure_val(cookie_val)

    def login(self, admin):
        self.set_secure_cookie('admin_id', str(admin.key.id()))

    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'admin_id=; Path=/')

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        admin_id = self.read_secure_cookie('admin_id')
        self.admin = admin_id and Admin.by_id(admin_id)

class AdminNewsHelper(AdminHandler):
    def store_file(self, file):
        bucket_name = app_identity.get_default_gcs_bucket_name()
        file_name = getattr(file, 'filename', None)
        file_content = getattr(file, 'file', None)

        if file_name and file_content:
            content_t = mimetypes.guess_type(file_name)[0]
            file_name = os.path.join('/', bucket_name, content_t, file_name)
            with cloudstorage.open(file_name, 'w', content_type=content_t) as f:
                f.write(file_content.read())
            return file_name        
        
class RegisterAdmin(AdminHandler):
    def get(self):
        self.delete_cookie('admin_id')
        code = self.request.get('code')
        params = dict()
        if code:
            invite = Invitation.by_code(code)
            if invite:
                params['name'] = invite.name
                params['email'] = invite.key.id()
                params['code'] = invite.code
                params['title'] = "Setup admin account"
                self.render('admin_signup.html', **params)
            else:
                self.write('Invitation already accepted')
        else:
            self.write('Invitation is required')

    def post(self):
        self.name = self.request.params.get('name')
        self.email = self.request.params.get('email')
        self.password = self.request.params.get('password')
        self.code = self.request.params.get('code')
        repassword = self.request.params.get('repassword')

        has_error = False
        params = dict(name = self.name,
                    email = self.email)

        if not valid_password(self.password):
            params['error_password'] = 'invalid password'
            has_error = True

        if not self.password == repassword:
            params['error_password'] = 'password did not match'
            has_error = True
        
        if not valid_username(self.name):
            params['error_name'] = 'invalid name'
            has_error = True

        if not valid_email(self.email):
            params['error_email'] = 'invalid email Address'
            has_error = True

        invite = Invitation.by_id(self.email)
        if not invite:
            params['error_email'] = 'sorry, you are not invited'
            has_error = True
        else:
            if invite.code != self.code:
                params['error_email'] = 'Invid signup process'
                has_error = True

        if has_error:
            self.render('admin_signup.html', **params)
            return

        admin = Admin.by_id(self.email)
        if admin:
            error_msg = 'email address in already in use'
            self.render('admin_signup.html', error_email = error_msg)
        else:
            admin = Admin.register(self.name, self.password, self.email)
            admin.put()
            self.login(admin)
            self.redirect('/admin/board')

class AdminBoard(AdminNewsHelper):
    def get(self):
        if not self.admin:
            self.logout()
            self.redirect('/admin/login')
        else:
            params = dict(admin = self.admin, 
                form_url = blobstore.create_upload_url(''), 
                title = "Admin board")
            self.render('admin_page.html', **params)

    def post(self):
        title = self.request.params.get('title')
        content = self.request.params.get('content')
        category = self.request.params.get('category')
        uploaded_file = self.request.POST.get('photo')
        uploaded_video = self.request.POST.get('video')

        file_name = self.store_file(uploaded_file)
        video_name = self.store_file(uploaded_video)

        news = Labari(
            author = self.admin.key.id(),
            title = title, 
            description = content,
            source = self.admin.name,
            category = category)
        if file_name : news.files.append(file_name)
        if video_name : news.files.append(video_name)
        news.put()
        self.redirect('/admin/board')

class AdminLogout(AdminHandler):
    def get(self):
        self.logout()
        self.redirect('/admin/login')

    def post(self):
        self.logout()
        self.redirect('/admin/login')

class AdminLogin(AdminHandler):
    def get(self):
        self.response.delete_cookie('admin_id')
        params = dict(title = 'Login to your Admin Account')
        self.render('admin_signin.html', **params)

    def post(self):
        email = self.request.params.get('email')
        password = self.request.params.get('password')

        admin = Admin.login(email, password)

        if admin:
            self.login(admin)
            self.redirect('/admin/board')
        else:
            msg = 'invalid login'
            self.render('admin_signin.html', error_msg = msg)

class SingleNews(HelperHandler):
    def get(self):
        id = int(self.request.url.split('/')[-1].strip())
        labari = Labari.by_id(id)
        if labari:
            params = dict(title = labari.title, labari = labari)
            self.render('single.html', **params)
        else:
            self.response.write('''
                <center>
                <h3>Yi Hakuri!!! Wannan Labari Da Kake nema babu shi.</h3>
                <h4><a href="/">Koma Baya</a></h4>
                </center>
                ''')

class SingleNewsAjax(HelperHandler):
    def get(self):
        id = self.request.url.split('/')[-1].strip()
        self.response.headers['Content-Type'] = 'application/json' 
        self.response.out.write(_get_single_news_ajax(id))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/api', MainHandlerAjax),
    ('/kgi43', InfiniteScroll),
    ('/about', AboutHandler),
    ('/contact', ContactHandler),
    ('/labari/[a-zA-Z0-9]+', SingleNews),
    ('/labari/ajax/[a-zA-Z0-9]+', SingleNewsAjax),
    ('/admin/signup', RegisterAdmin),
    ('/admin/login', AdminLogin),
    ('/admin/logout', AdminLogout),
    ('/admin/invite', AdminInvite),
    ('/admin/board', AdminBoard),
    ('/ka385ni9ka87rihalutr8e7iauhe7a8w4iagof87toa47a8fgoyi8a4798qtr47fg8aw47gfo847tgf8oyr47love8tag8o478of7t8w49itr7i8you46ti4', GetNewsHandler),
])