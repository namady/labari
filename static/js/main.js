$(document).ready(function() {
   $(".raw-time").each(function() {
        var html = $(this).html();
        var fhtml = moment(html).fromNow();
        if(fhtml === 'Invalid date') {
            return;
        }
        $(this).html(fhtml);
        $(this).removeClass('raw-time')
    });

    $('.rfi-image').foggy({
       blurRadius: 2,          // In pixels.
       opacity: 0.8,           // Falls back to a filter for IE.
       cssFilterSupport: true  // Use "-webkit-filter" where available.
     });

    var infScroll = new InfiniteScroll( '#news-container', {
        path: function() { return "/kgi43" },
        append: '.single-news',
        history: false,
        scrollThreshold: 800,
    });

    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('nav').addClass('shrink');
        } else {
            $('nav').removeClass('shrink');
        }

        $(".raw-time").each(function() {
            var html = $(this).html();
            var fhtml = moment(html).fromNow();
            if(fhtml === 'Invalid date') {
                return;
            }
            $(this).html(fhtml);
            $(this).removeClass('raw-time')
        });      
    });

    $(".navbar-brand").click(function(e) {
        window.location = 'https://atakaice.com';
    });

    $("#sendMessage").submit(function(e) {
        $('button[type="submit"]').prop('disabled', true);
        e.preventDefault();
        $.ajax({
            type : "POST",
            url : '/',
            data : $('form').serialize(),
            success : function(response) {
                toastr.success('Sakon ya tafi');
                $("#sendMessage")[0].reset();
                $('button[type="submit"]').prop('disabled', false);
            },
            error : function(response) {
                toastr.error('Akwai matsala wurin tura wannan sakon!');
                $('button[type="submit"]').prop('disabled', false);
            }
        })
    });

    $("#sendInvitation").submit(function(e) {
        e.preventDefault();
        $('button[type="submit"]').prop('disabled', true);
        $.post({
            url : $(this).attr('action'),
            data : $(this).serialize(),
            success : function(response) {
                toastr.success(response.status_msg);
                $("#inviteModal").modal('hide');
                $("#sendInvitation")[0].reset();
                $('button[type="submit"]').prop('disabled', false);
            },
            error : function(response) {
                toastr.error(response.responseJSON.status_msg);
                $('button[type="submit"]').prop('disabled', false);
            }
        })
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= (param * 1000000))
    }, 'File size must not be more than {0} MB');

    $( ".admin-form" ).validate({
      rules: {
        photo: {
          required: true,
          extension: "jpg|gif|png",
          filesize: 5
        },

        video: {
          required: true,
          extension: "mp4"
        },

        email: {
            email:true
        },

        "title": {
            required: true,
        },

      }
    });
});
