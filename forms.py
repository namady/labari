import re
import hmac
import random
import hashlib
from string import letters

secret = 'crimson'

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,50}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return email and EMAIL_RE.match(email)


def make_salt(length = 5):
    return ''.join(random.choice(letters) for x in xrange(length))

def make_pw_hash(email, password, salt = None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(email + password + salt).hexdigest()
    return '%s,%s' % (salt, h)

def validate_user_password(email, password, hashed_password):
    salt = hashed_password.split(',')[0]
    return hashed_password == make_pw_hash(email, password, salt)

def make_secure_val(val):
	return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
	val = secure_val.split('|')[0]
	if secure_val == make_secure_val(val):
		return val