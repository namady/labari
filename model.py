from google.appengine.ext import ndb
from google.appengine.ext import db
import xml.etree.ElementTree as ET
from datetime import datetime
import urllib, urllib2
import feedparser
from threading import Thread
from google.appengine.ext.blobstore import blobstore
from twitter.api import Twitter
import os

#from enum import Enum

#from bs4 import BeautifulSoup
#from appengine_config import post_to_instagram
from appengine_config import translate

# class Labari(ndb.Model):
# 	title = ndb.StringProperty(required=True)
# 	description = ndb.TextProperty(required=True)
# 	link = ndb.StringProperty(required=True)
# 	pubDate = ndb.DateTimeProperty(required=True)
# 	image = ndb.StringProperty(required=True)
# 	source = ndb.StringProperty(required=True)

# class NewsCategory(Enum):
# 	ENTERTAINMENT = 'ENTERTAINMENT'
# 	SPORT = 'SPORT'
# 	POLITICS = 'POLITICS'
# 	BUSINESS = 'BUSINESS'
# 	SCIENCE = 'SCIENCE'
# 	TECHNOLOGY = 'TECHNOLOGY'
# 	EDUCATION = 'EDUCATION'

class Labari(ndb.Model):
	title = ndb.StringProperty(required=True)
	description = ndb.TextProperty(required=False)
	link = ndb.StringProperty(required=False)
	files = ndb.StringProperty(repeated=True)
	category = ndb.StringProperty(required=True, default='General')
	pubDate = ndb.DateTimeProperty(auto_now_add=True)
	updated_at = ndb.DateTimeProperty(auto_now=True)
	source = ndb.StringProperty(required=True)
	author = ndb.StringProperty(required=False)

	@classmethod
	def by_id(cls, uid):
		return Labari.get_by_id(uid)

class News(ndb.Model):
	title = ndb.StringProperty(required=False)
	content = ndb.StringProperty(required=False)
	files = ndb.StringProperty(repeated=True)
	category = ndb.StringProperty(required=True, default='Entertainment')
	pubDate = ndb.DateTimeProperty(auto_now_add=True)
	updated_at = ndb.DateTimeProperty(auto_now=True)

def _store_news():
	channels = {}
	channels['rfi'] = "http://ha.rfi.fr/general/rss"
	channels['voa'] = "http://www.voahausa.com/api/zggkme__gm"
	# channels['dw'] = "http://rss.dw.com/rdf/rss-hau-nr"
	# channels['dw2'] = "http://partner.dw.com/rdf/rss-hau-pol"

	_store_bbc()

	for channel in channels:
		current_feeds = feedparser.parse(channels[channel])
		for feed in current_feeds["items"]:
			querry = Labari.gql("WHERE link = :1", feed["link"])
			if querry.count() == 0:
				title = feed['title_detail']['value']
				tmp = feed['summary_detail']['value']
				tmp = tmp.replace('\n', '')
				description = tmp
				link = feed['link']
				pubDate = feed['updated']
				image = feed['enclosures'][0]['href']
				if channel == 'voa': 
					source = 'voa'
				else: 
					source = 'rfi'
				if title and link and pubDate and image and description:
					_put_to_database(title, description, pubDate, source, link, image)

def _store_bbc():
	data = urllib.urlopen("http://feeds.bbci.co.uk/hausa/rss.xml")
	response = data.read()
	root = ET.fromstring(response)

	for neighbor in root.iter('item'):
		xlink = neighbor.find('link').text
		querry = Labari.gql("WHERE link = :1", xlink)
		if querry.count() == 0:
			title = neighbor.find('title').text
			description = neighbor.find('description').text
			pubDate = neighbor.find('pubDate').text
			x = neighbor.find('{http://search.yahoo.com/mrss/}thumbnail')
			if x is None:
				continue
			else:
				image = x.attrib['url']
			link = xlink
			source = 'bbc'
			if title and link and pubDate and image and description:
				_put_to_database(title, description, pubDate, source, link, image)

def _put_to_database(t, d, p, s, l, i):
	item = Labari(
		author = s,
		title = t, 
		description = d,
		source = s.upper(),
		link = l)
	item.files.append(i)
	item.put()
	if not os.environ['APPLICATION_ID'].startswith('dev'):
		Thread(target=_post_to_facebook, args=[item]).start()
		Thread(target=_post_to_twitter, args=[item]).start()

def _post_to_facebook(item):
	description = item.description + " - " + item.source
	access_token = "EAAPQcZCRY5rIBANPhngSJaz2BfDhSwmZAOVOd6hRZCSR6rQF2oNqZB81CdGrDP1nKNnoP2ZAuUEtlHvjiMBLKjckiW7QZCePKANrrx4GuFxUPtK08BnosOzuZBUEX6p7MdCHdVg7bIt08RbE02oOkp1Kit0tF35ynnW3ChZCqSXRNAZDZD";
	data = urllib.urlencode({'access_token': access_token,
							'link': "http://atakaice.com/labari/" + str(item.key.id())})
	urllib2.urlopen("https://graph.facebook.com/755864664566598/feed", data)

def _post_to_twitter(item):
	twitter = Twitter('atakaice','Muawiyyah555')
	twitter.statuses.update(status = "http://atakaice.com/labari/" + str(item.key.id()))

def _post_to_facebook2(item):
	item_link = "http://atakaice.com/labari/" + str(item.key().id())
	description = item.description.encode('utf-8').strip() + " - " + item.source + "\n" + "cikakken labari\n" + item_link
	access_token = "EAAPQcZCRY5rIBANPhngSJaz2BfDhSwmZAOVOd6hRZCSR6rQF2oNqZB81CdGrDP1nKNnoP2ZAuUEtlHvjiMBLKjckiW7QZCePKANrrx4GuFxUPtK08BnosOzuZBUEX6p7MdCHdVg7bIt08RbE02oOkp1Kit0tF35ynnW3ChZCqSXRNAZDZD";
	data = urllib.urlencode({'access_token': access_token,
							'url': item.image,
							'caption': description})
	urllib2.urlopen("https://graph.facebook.com/755864664566598/photos", data)
	#post_to_instagram(description, item.image)

def process_date(date):
	months = {'Jan':1, 'Feb':2, 'Mar':3, "Apr":4, 'May':5, 'Jun':6, 'Jul':7, 'Aug':8, 'Sep':9, 'Oct':10, 'Nov':11, 'Dec':12}
	date_number = date[5:8]
	month_name = date[8:11]
	year = date[12:16]
	hour = date[17:19]
	minute = date[20:22]
	return datetime(int(year), int(months[month_name]), int(date_number), int(hour), int(minute), int(00))